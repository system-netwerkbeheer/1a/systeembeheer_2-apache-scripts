#!/usr/bin/env bash
client=$1
shift

# [default flag vars]
progname=$(basename $0)
remote_user=root
remote_ca_switch=0
certificate=/srv/ca/cacert.pem
ca_srv=
# -------

client_cert_loc=/usr/local/share/ca-certificates/extra

# ================ #
# |flags|
# ================ #
# taken and modified from: https://stackoverflow.com/a/39376824

# define help
display_help(){
   cat << END

   Usage: $progname <client-ip> [options]...

   optional arguments:
     -h, --help               show this help message and exit

     -c, --cert FILE          location of root '.pem' certificate (default: /srv/ca/cacert.pem)
     -u, --remote-user USER   login to the given user for the remote client (instead of root)
                              script will make sure that the given user has sufficient premissions to write in "/usr/local/share/ca-certificates/extra"

     --remote-ca IP           enables use of remote ca-server at given IP (note that script expects to be logged in as root)

END
# -v, --verbose        increase the verbosity of the bash script (shows sterr)
}
# use getopt and store the output into $OPTS
# note the use of -o for the short options, --long for the long name options
# and a : for any option that takes a parameter
OPTS=$(getopt -o "hc:u:" --long "help,cert:,remote-user:,remote-ca:" -n "$progname" -- "$@")
if [ $? != 0 ] ; then echo "Error in command line arguments." >&2 ; display_help; exit 1 ; fi
eval set -- "$OPTS"

while true; do
  # uncomment the next line to see how shift is working
  # echo "\$1:\"$1\" \$2:\"$2\""
  case "$1" in
    -h | --help ) display_help; exit; ;;
    -c | --cert ) certificate=$2; shift 2 ;;
    -u | --remote-user ) remote_user=$2; shift 2 ;;
    --remote-ca ) remote_ca_switch=1 ca_srv="$2"; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done
# -------
hush_remote_motd(){
	# remove the motd on remote ssh, so output is clearer
	ssh $remote_user@$client "touch $HOME/.hushlogin" > /dev/null 2>&1  # output of this is send to /dev/null as we do not care about it
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1
	fi
}

cleanup_hush_remote_motd(){
	# remove the .hushlogin files so that motd is displayed should root login again
	ssh $remote_user@$client "rm $HOME/.hushlogin" > /dev/null 2>&1
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1
	fi
}

convert_pem_to_csr(){
    if [ "$remote_ca_switch" == "1" ]; then
        ssh root@$ca_srv "openssl x509 -in $certificate -inform PEM -out $1"
    else
        openssl x509 -in $certificate -inform PEM -out $1
    fi
}

mv_cert_to_client(){
    if [ "$remote_ca_switch" == "1" ]; then
        ssh -t $remote_user@$client "sudo mkdir -p ${client_cert_loc}; sudo chown root:$USER ${client_cert_loc}; sudo chmod 775 ${client_cert_loc}"
        # -t creates a terminal on remote so that sudo can work
        scp -3 scp://root@$ca_srv:$1 scp://$remote_user@$client:${client_cert_loc}/newco_root.crt
        # scp -3 allows to route files between 2 remote systems via a third system, from: https://superuser.com/a/1380427
        ssh -t $remote_user@$client "sudo chown root:root ${client_cert_loc}/newco_root.crt"
    else
        ssh -t $remote_user@$client "sudo mkdir -p ${client_cert_loc}; sudo chown root:\$USER ${client_cert_loc}; sudo chmod 775 ${client_cert_loc}"
        scp $1 $remote_user@$client:${client_cert_loc}/newco_root.crt
        ssh -t $remote_user@$client "sudo chown root:root ${client_cert_loc}/newco_root.crt"
    fi
}

inform_client(){
    ssh -t $remote_user@$client 'sudo update-ca-certificates'
}
if [ ! "$client" ] || [[ $client == -* ]]; then
    echo "you must at least supply a valid client ip"
    display_help
    exit 1
elif [ "$remote_ca_switch" == "1" ] && [ ! "$ca_srv" ] ; then
    echo "remote CA ip is missing"
    display_help
    exit 1
else
    tmp_crt=$(mktemp /tmp/crt.XXXXXXXXX)
    # mktemp creates a file with a random name in /tmp

    convert_pem_to_csr "$tmp_crt"
    hush_remote_motd
    mv_cert_to_client "$tmp_crt"
    inform_client
    cleanup_hush_remote_motd
fi
