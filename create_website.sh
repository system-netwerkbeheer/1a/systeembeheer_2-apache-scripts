#!/usr/bin/env bash
# i'm probs gonna overcomplicate this...

# [install dependancy if it does not exist]
# whois (for mkpasswd command)
#if [ $(dpkg-query -W -f='${Status}' whois 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
#	apt-get install -y whois
#fi # from: https://stackoverflow.com/a/22592801
# --> not needed as it does not work as intended (openssl does not understand the generated output)
# ================ #
# |input|
# ================ #
dom=$1
shift # moves the $? array to the left ($2 becomes the new $1, which is then moved off the array, see: https://unix.stackexchange.com/a/174568)
# make sure domain does not start with "www." by removing the string if it does (to prevent duplication)
if [[ $dom == www.* ]]; then
	dom="${dom//www.}"
fi

# [default flag vars]
progname=$(basename $0)
yes_switch=0
remote_ca_switch=0
#verbose_switch=0
ca_srv=
# -------

# ================ #
# |global vars|
# ================ #

# [dns]
ns1=192.168.0.20
ns1_lastdigits=$(echo $ns1 | sed -r 's!/.*!!; s!.*\.!!')
ns1_rip="${ns1//$ns1_lastdigits}"
ns2=192.168.0.21
ns2_lastdigits=$(echo $ns2 | sed -r 's!/.*!!; s!.*\.!!')
ns2_rip="${ns2//$ns2_lastdigits}"
ns_etc=/etc/bind
fnamed=${ns_etc}/named.conf.local
soa_serial=$(date "+%s") # %s is the UTC timestamp (number of seconds since 1970-01-01 00:00:00 UTC), this should allow easy updating of SOA, so long as you do not spam the script every .5 seconds...
# [local]
ip=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/') # from https://unix.stackexchange.com/a/119272
lastdigits=$(echo $ip | sed -r 's!/.*!!; s!.*\.!!') # from "I forgot to copy-paste the stackoverflow comment url".com
rip="${ip//$lastdigits}" # from https://unix.stackexchange.com/a/311762
# [apache location vars]
fhtml=/var/www/html
fdom=${fhtml}/${dom}
fa_available=/etc/apache2/sites-available
fa_enabled=/etc/apache2/sites-enabled
# [ssl locations]
ssl_cnf=/usr/lib/ssl/openssl.cnf
ca=/srv/ca
privatedir=/etc/apache2/ssl/private
publicdir=/etc/apache2/ssl/public
#cert_password=$(mkpasswd -m sha512crypt --salt "$soa_serial" --stdin) 
# -> does not work as intended, from: https://www.reddit.com/r/bash/comments/v6d33t/how_to_avoid_passwords_in_clear_text_in_a_bash/ibfgt31/
# --> the goal was to use this as genrsa -passout:$cert_password and req -passin pass:$cert_password (from: https://serverfault.com/a/1050596) but the format of mkpasswd does not work with this. 
# -------

# ================ #
# |flags|
# ================ #
# taken and modified from: https://stackoverflow.com/a/39376824

# define help
display_help(){
   cat << END

   Usage: $progname <domain> [options]...

   optional arguments:
     -h, --help           show this help message and exit
     -y                   answers all yes/no questions with yes
                          (WILL OVERWRITE ALL EXISTING FILES (db.zone and apache configs)!)

     --remote-ca IP       enables use of remote ca-server at given IP (note that script expects to be logged in as root)

END
# -v, --verbose        increase the verbosity of the bash script (shows sterr)
}
# use getopt and store the output into $OPTS
# note the use of -o for the short options, --long for the long name options
# and a : for any option that takes a parameter
OPTS=$(getopt -o "hy" --long "help,remote-ca:" -n "$progname" -- "$@")
if [ $? != 0 ] ; then echo "Error in command line arguments." >&2 ; display_help; exit 1 ; fi
eval set -- "$OPTS"

while true; do
  # uncomment the next line to see how shift is working
  # echo "\$1:\"$1\" \$2:\"$2\""
  case "$1" in
    -h | --help ) display_help; exit; ;;
    --remote-ca ) remote_ca_switch=1 ca_srv="$2"; shift 2 ;;
    -y ) yes_switch=1; shift ;;
    #-v | --verbose ) verbose_switch=1 ; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done
# -------

# vars are lowercase as per: https://stackoverflow.com/a/673940


# ================ #
# |functions|
# ================ #
is_not_root(){
	! [ "$(id -u)" -eq 0 ]
	# check if command is not run as root (or via sudo), taken from: https://stackoverflow.com/a/52586842
}

is_valid(){
	# is_valid = function that checks if the domain is a valid FQDN
		
	echo $dom | grep -P '(?=^.{4,253}$)(^(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,}|xn--[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])$)' || return 1
	# grep checks if FQDN is valid (source: https://stackoverflow.com/a/26850032, regex is black magic)
}

ask_yes_or_no(){
# a little bit of safety
    read -p "$1 (y/N): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
    # I could use is_valid to add conditions like -y that change this to always say yes (if [[ $1 == "-y" ]]; then \ change $reply to "yes" bla bla), but that means also somehow skipping the questions below, so ....
    # update: done
} # complete function from: https://stackoverflow.com/a/16508612

create_new_log_entry(){
	printf '\n[log_%s for domain: %s]\n' "$(date '+%D_%H:%M:%S')" "$dom" >> create_website.log
	printf '\n[log_%s for domain: %s]\n' "$(date '+%D_%H:%M:%S')" "$dom" >> create_website_error.log
}

apache_not_exists(){
	! [ -d "/var/www/html/${dom}" ]
}

ssl_cert_not_exists(){
	! [ -f "${publicdir}/${dom}.pem" ]
}
create_apache(){
	# create input domain in html folder, then echo start page information ("Dit is de website voor mydomain.be.")
	mkdir $fdom; printf 'Dit is de website voor %s.\n' " ${dom}" > ${fdom}/index.html

	# create the apache configuration file in /etc/apache2/sites-available using printf (mostly using default config)
	# \n means new line, \t means tab, %s will input variables in order given like "$var1" "$var2" that is after 'text %s %s'

	printf '<VirtualHost *:80>\n\tServerAdmin admin@%s\n\tDocumentRoot %s\n\tServerName %s\n\tServerAlias www.%s\n\tErrorLog ${APACHE_LOG_DIR}/error.log\n\tCustomLog ${APACHE_LOG_DIR}/access.log combined\n</VirtualHost>\n' "$1" "$fdom" "$dom" "$dom" > ${fa_available}/${dom}.conf

	# modern website should probably redirect all http traffic to https, like in https://linuxize.com/post/redirect-http-to-https-in-apache/, wont do it here cuz testing
}

create_ssl(){
	# note that this assumes root CA is configured beforehand
	if [ ! -d "$privatedir" ]; then
		mkdir -p ${privatedir}
		chmod -R 750 ${privatedir}
		chown -R root:ssl-cert ${privatedir}
	fi

	openssl genrsa -out ${privatedir}/${dom}.key 4096
	chown root:ssl-cert ${privatedir}/${dom}.key
	chmod 640 ${privatedir}/${dom}.key

	# uncomment copy_extensions = copy as per comment under https://security.stackexchange.com/a/183973
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "sed -i '/copy_extensions = copy/s/^#//g' $ssl_cnf"
	else
		sed -i '/copy_extensions = copy/s/^#//g' $ssl_cnf
	fi # sed from: https://stackoverflow.com/a/27355109

	openssl req -new -key ${privatedir}/${dom}.key -subj /CN=${dom}/emailAddress=admin@${1}/C=BE/ST=Oost-Vlaanderen/L=Gent/O=NEWCO/ -addext "subjectAltName =  DNS:${dom}, DNS:www.${dom}" -out ${privatedir}/${dom}.csr
	# addext subjectaltname allows for multible domains to be linked to a single certificiate (in tis case dom and www.dom) additional domains can be added by ,DNS:domain2.com, DNS:www.domain2.com, ...
	# --> from: https://serverfault.com/a/680311 (was getting mad trying to find this lol, note that the uncomment of copy_extentions line in ssl_cnf is needed for this to function, at least to my testing)
	
	chown root:ssl-cert ${privatedir}/${dom}.csr
	chmod 640 ${privatedir}/${dom}.csr

	if [ ! -d "$publicdir" ]; then
		mkdir -p ${publicdir}
		chmod -R 755 ${publicdir}
		chown -R root:ssl-cert ${publicdir}
	fi

	if [ "$remote_ca_switch" == "1" ]; then
		# use scp to put the generated csr into remote if using a remote ca server -->
		scp ${privatedir}/${dom}.csr root@$ca_srv:${ca}/requests/${dom}.csr
		# ssh into remote ca server instead if not local -->
		ssh root@$ca_srv "openssl ca -in ${ca}/requests/${dom}.csr -out ${ca}/certs/${dom}.pem"
		# scp the pem file to local if using a remote ca server -->
		scp root@$ca_srv:${ca}/certs/${dom}.pem ${publicdir}/${dom}.pem
	else
		openssl ca -in ${privatedir}/${dom}.csr -out ${publicdir}/${dom}.pem
	fi

	chmod 644 ${publicdir}/*${dom}.pem

	# recomment copy_extensions = copy to clean up configuration (idk if this is needed)
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "sed -i '/copy_extensions = copy/s/^/#/g' $ssl_cnf" # sed from: https://stackoverflow.com/a/27355109
	else
		sed -i '/copy_extensions = copy/s/^/#/g' $ssl_cnf
	fi # sed from: https://stackoverflow.com/a/27355109
}

create_apache_ssl(){
	printf '<VirtualHost *:443>\n\tServerAdmin admin@%s\n\tDocumentRoot %s\n\tServerName %s\n\tServerAlias www.%s\n\tErrorLog ${APACHE_LOG_DIR}/error.log\n\tCustomLog ${APACHE_LOG_DIR}/access.log combined\n\tSSLEngine on\n\tSSLCertificateFile %s\n\tSSLCertificateKeyFile %s\n</VirtualHost>\n' "$1" "$fdom" "$dom" "$dom" "${publicdir}/${dom}.pem" "${privatedir}/${dom}.key" >> ${fa_available}/${dom}.conf
}

enable_apache(){
	# enable site with ln
	ln -s ${fa_available}/${dom}.conf ${fa_enabled}/${dom}.conf
	systemctl restart apache2.service
} # must resist urge to make more functions

hush_remote_motd(){
	# remove the motd on remote ssh, so output is clearer
	ssh root@$ns1 "touch $HOME/.hushlogin" > /dev/null 2>&1  # output of this is send to /dev/null as we do not care about it
	ssh root@$ns2 "touch $HOME/.hushlogin" > /dev/null 2>&1 
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1 
	fi

}

cleanup_hush_remote_motd(){
	# remove the .hushlogin files so that motd is displayed should root login again
	ssh root@$ns1 "rm $HOME/.hushlogin" > /dev/null 2>&1 
	ssh root@$ns2 "rm $HOME/.hushlogin" > /dev/null 2>&1 
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1 
	fi
}

extract_zone(){
    echo "$1" | grep -Eo '[^.]*.[^.]*$'
} # from https://unix.stackexchange.com/a/711621

extract_reverse_zone_ns1(){
	# --> needed for if the dns is on a different subnet, wont add conditions for it but I thought about it!
	echo "$(printf %s "$ns1_rip" | tac -s.)in-addr.arpa"
} # from https://unix.stackexchange.com/a/132785

extract_reverse_zone(){
	echo "$(printf %s "$rip" | tac -s.)in-addr.arpa"
} # from https://unix.stackexchange.com/a/132785

extract_reverse_ip(){
	# same as extract_reverse_zone but without the .in-addr.arp addendum, makes it easier to delete reverse ip db files
	echo "$(printf %s "$rip" | tac -s.)" | sed 's/.$//'
	# sed command removes last character in the string (aka the trailing ".")
} # sed command is from: https://unix.stackexchange.com/a/229994

extract_subdom(){
	echo "$dom" | sed -e "s#.${1}##"
} # sed command modified from: https://stackoverflow.com/a/17129081

zone_exists(){
	# dig @${ns1} $1 >/dev/null 2>&1 -> seems to detect if the zone exists upstream, so ill just check if the file exists
	ssh -q root@$ns1 [[ -f ${ns_etc}/db.${1} ]]
}

reverse_zone_exists(){
	ssh -q root@$ns1 [[ -f ${ns_etc}/db.${1} ]]
}

subdom_exists(){
	ssh -q root@$ns1 "grep -q '${1} IN A' ${ns_etc}/db.${2}"
}

delete_zone(){
	# <<+ is a delimiter that will push all text until + into ssh as commands line by line, reason i needed this is cuz there is a lot of ' and " which made the normal method not work
	# the sed command removes the domain-zone from /etc/bind/named.conf.local
	ssh -T root@$ns1 <<+
		sed -nie "/\"$1\"/,/^\};"'$/d;p;' $fnamed
+
	ssh -T root@$ns2 <<+
		sed -nie "/\"$1\"/,/^\};"'$/d;p;' $fnamed
+
} # method for delimiting from: https://stackoverflow.com/a/27413675; first sed command from: did not save source :(

delete_subdom() {
	ssh -T root@$ns1 << +
		grep -v "${2} IN A\|www.${dom}. IN CNAME ${dom}." ${ns_etc}/db.${1} > /tmp/db.${1}.tmp; mv /tmp/db.${1}.tmp ${ns_etc}/db.${1}
+
	# the grep command will print the file without "pattern", we put that in tmp and then mv tmpfile to replace the old file
} # grep modified from: https://stackoverflow.com/a/13188531

delete_reverse_subdom(){
	ssh -T root@$ns1 << +
		grep -v "IN PTR ${dom}." ${ns_etc}/db.${1} > /tmp/db.${1}.tmp; mv /tmp/db.${1}.tmp ${ns_etc}/db.${1}
+
} # grep modified from: https://stackoverflow.com/a/13188531

create_new_zone(){
	# create zone in named.conf.local
	# used cat command, otherwise I could've used printf like:
	# printf 'zone "%s" {\n\ttype master;\n\tfile "${ns_etc}/db.%s";\n\tallow-transfer { 192.168.0.21; };\n\talso-notify { 192.168.0.21; }\n};\n' "$1" "$1" >> $fnamed

	# --> note that you can actually use <<- to ignore the leading tabs but not the spaces, idk using spaces is annoying so that why formatting not nice
	ssh -T root@$ns1 << + 
	tee -a $fnamed << 'END'
zone "${1}" {
	type master;
	file "${ns_etc}/db.${1}";
	allow-transfer { ${ns2}; };
	also-notify { ${ns2}; };
};
END
+
	ssh -T root@$ns2 << + 
	tee -a $fnamed << 'END'
zone "${1}" {
	type slave;
	file "/var/cache/bind/db.${1}";
	masters { ${ns1}; };
};
END
+
}

create_new_reverse_zone(){
	# same as create_new_zone but with revers zone and reverse ip
	ssh -T root@$ns1 << + 
	tee -a $fnamed << 'END'
zone "${1}" IN {
	type master;
	file "${ns_etc}/db.${1}";
	allow-transfer { ${ns2}; };
	also-notify { ${ns2}; };
};
END
+
	ssh -T root@$ns2 << + 
	tee -a $fnamed << 'END'
zone "${1}" IN {
	type slave;
	file "/var/cache/bind/db.${1}";
	masters { ${ns1}; };
};
END
+
}

create_new_records(){
	# need to put $TTL first into local var as for some reason it starts with a $ and this breaks the things
	# firstline=$(printf '$TTL\t86400\n')  # OH NO NOT IMPURE FUNCTIONS --> can also escape variables via \$var like in https://stackoverflow.com/a/14708253
	ssh -T root@$ns1 << +  
	tee ${ns_etc}/db.${1} << 'END'
\$TTL	86400
@	IN	SOA	ns1.${1}.	admin.${1}.	${soa_serial} 604800 86400 2419200 86400
;
@   IN  NS  ns1.${1}.
@   IN  NS  ns2.${1}.
ns1 IN  A   $ns1
ns2 IN  A   $ns2
;
$2 IN A $ip
www.${dom}. IN CNAME ${dom}.
END
+
}

create_new_reverse_db(){
	#firstline=$(printf '$ORIGIN\t%s\n$TTL\t86400\n' "$1")
	# ok I must've misunderstood how this works, how am I supposed to create a reverse zone if there are multiple seperate forward zones in that subnet?
	# --> changed ns*.${2} to ns*.newco.local, but idk if this is what I am supposed to do...
	if [ "$1" == "$3" ]; then
		ssh -T root@$ns1 << +
		tee ${ns_etc}/db.${3} << 'END'
\$ORIGIN	${1}.
\$TTL	86400
@	IN	SOA	ns1.newco.local.	admin.newco.local.	${soa_serial} 604800 86400 2419200 86400
	IN	NS	ns1.newco.local.
	IN	NS	ns2.newco.local.
$ns1_lastdigits IN PTR ns1.newco.local.
$ns2_lastdigits IN PTR ns2.newco.local.
;
END
+
	else
		ssh -T root@$ns1 << +
		tee ${ns_etc}/db.${3} << 'END'
\$ORIGIN	${3}.
\$TTL	86400
@	IN	SOA	ns1.newco.be.	admin.newco.be.	${soa_serial} 604800 86400 2419200 86400
	IN	NS	ns1.newco.be.
	IN	NS	ns2.newco.be.
$ns1_lastdigits IN PTR ns1.newco.be.
$ns2_lastdigits IN PTR ns2.newco.be.
\$ORIGIN	${1}.
\$TTL	86400
@	IN	SOA	ns1.${2}.	admin.${2}.	${soa_serial} 604800 86400 2419200 86400
	IN	NS	ns1.${2}.
	IN	NS	ns2.${2}.
;
END
+
	fi
} # layout taken from: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/reference_guide/s2-bind-configuration-zone-reverse

append_records(){
	ssh -T root@$ns1 << + 
	tee -a ${ns_etc}/db.${1} << 'END'
${2} IN A $ip
www.${dom}. IN CNAME ${dom}.
END
+
}

append_reverse_records(){
	ssh -T root@$ns1 << + 
	tee -a ${ns_etc}/db.${1} << 'END'
$lastdigits IN PTR ${dom}.
END
+
}

refresh_SOA_serial(){
	# the SOA record serial number needs to be updated every time the db.zone file is modified so that the slave servers update their cache

	# 	if '' are put arround the delimeter like '+' the variables would not be able to expand -> $1 would litteraly be printed instead of example.net, double quotes or no quotes do allow expansion

	ssh -T root@$ns1 << +
		awk '{ if ( \$0 ~ /[\t ]SOA[\t ]/ ) \$6=$soa_serial; print}' ${ns_etc}/db.${1} > /tmp/updated_record && mv /tmp/updated_record ${ns_etc}/db.${1}
		awk '{ if ( \$0 ~ /[\t ]SOA[\t ]/ ) \$6=$soa_serial; print}' ${ns_etc}/db.${2}> /tmp/updated_rr_record && mv /tmp/updated_rr_record ${ns_etc}/db.${2}
+
	# awk command will replace the 6th field in the line that contains the word SOA, surrounded by spaces or tabs (so the serial number), by the UTC timestamp ($soa_serial)
	# it assumes that the SOA entry will be on 1 line, therefore the formatting of db file must be correct
	# --> so soa entry must be: <name> <class> <type> <mname> <rname> <serial> <refresh> <retry> <expire> <minimum>

	# awk command cannot edit the file directly? --> didnt find a good answer to this, should read some things about awk but the book I have is a 1000 pages long so fuck that
	# --> function now creates a new file in /tmp and then replaces the original
} # awk command from: https://serverfault.com/a/950021

restart_remote_named(){
	ssh root@$ns1 'systemctl restart named.service' 
	ssh root@$ns2 'systemctl restart named.service' 
}
# -------
# must resist creating more functions....

# ==================== #
# |main function|
# ==================== #
main(){
	# [dns vars]
	zone=$(extract_zone "$dom")
	reverse_zone=$(extract_reverse_zone)
	ns1_reverse_zone=$(extract_reverse_zone_ns1)
	reverse_ip=$(extract_reverse_ip)
	subdom=$(extract_subdom "$zone")
	# -> I've put these here so that the functions are only run when the error checks return false (aka only when main is run and they are needed)
	# ---

	# hush remote login motd
	hush_remote_motd
	
	# make sure that certificate exists for the given domain
	if ssl_cert_not_exists; then
		printf "creating ssl keys and certificates, script may pause for a few seconds...\n\n" 2>&1
		create_ssl "$zone"
	fi

	if apache_not_exists; then
		create_apache "$zone"
		# setup $dom SSL + restart apache service
		create_apache_ssl "$zone"
		enable_apache
		printf "apache config created/enabled\n\n" 2>&1
	else
		echo -e "$fdom exists\n"

		if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to overwrite it?") ]]; then
			printf "ok skipping to dns configuration\n\n" 2>&1
		else
			create_apache "$zone"
			# setup SSL
			if ssl_cert_not_exists; then # this will keep reusing certificates, idk if this is good practice, probably not...
				create_ssl "$zone"
			fi
			create_apache_ssl "$zone"
			enable_apache
			printf "apache config overwritten/enabled\n\n" 2>&1
		fi
	fi

	# setup dns configuration
	# determine if given domain has a subdomain  and if it exists
	if [ "$subdom" = "$dom" ] && zone_exists "$zone"; then
		# make sure zone is not repeated in named.conf.local
		delete_zone "$zone"
		# make sure zone exists in named.conf.local
		create_new_zone "$zone"

		if subdom_exists "@" "$zone"; then
			echo -e "A record for $subdom already exists in db.$zone\n" 2>&1
			if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to replace the entire db.$zone? (THIS DELETES ALL EXISTING SUBDOMAINS)") ]]; then
				printf "removing old record from db\n\n" 2>&1
				delete_subdom "$zone" "@"
				delete_reverse_subdom "$reverse_zone"
				printf "appending new record to db\n\n" 2>&1
				append_records "$zone" "@"
				append_reverse_records "$reverse_zone"
				printf "record installed\n\n" 2>&1
			else
				echo -e "overwriting existing db.$zone\n"
				create_new_records "$zone" "@"
				delete_reverse_subdom "$reverse_zone"
				append_reverse_records "$reverse_zone"
				refresh_SOA_serial "$zone" "$reverse_zone"
				restart_remote_named
			fi
		else
			echo -e "overwriting existing db.$zone\n" 2>&1
			if reverse_zone_exists "$reverse_zone"; then
				delete_reverse_subdom "$reverse_zone"
				append_reverse_records "$reverse_zone"
			else
				# make sure that reverse_zone is not repeated in /etc/bind/named.conf.local
				delete_zone "$reverse_zone"
				# recreate zone
				create_new_reverse_zone "$reverse_zone" "$zone"
				create_new_reverse_db "$reverse_zone" "$zone" "$ns1_reverse_zone"
				append_reverse_records "$reverse_zone"
			fi
			create_new_records "$zone" "@"
			refresh_SOA_serial "$zone" "$reverse_zone"
			restart_remote_named
		fi
	# else if the zone exists either replace the entire zone file or append the new subdom to the zone
	elif zone_exists "$zone"; then
		# make sure zone is not repeated in named.conf.local
		delete_zone "$zone"
		# make sure zone exists in named.conf.local
		create_new_zone "$zone"

		printf "subdomain of existing dns zone detected\n\n" 2>&1
		if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to replace the entire db.$zone? (THIS DELETES ALL EXISTING SUBDOMAINS)") ]]; then
			if subdom_exists "$subdom" "$zone"; then
				echo -e "A record for $subdom already exists in db.$zone\n" 2>&1
				if [[ "no" == $(ask_yes_or_no "Do you wish to overwrite this subdomain?") ]]; then # yes switch is not needed as it must already be 0
				# is probably excessive, but might be handy if subdomains use different ip's (would need to add a flag for that...)
					printf "ok skipping dns configuration\n\n" 2>&1
				else
					printf "removing old record from db\n\n" 2>&1
					delete_subdom "$zone" "$subdom"
					delete_reverse_subdom "$reverse_zone"
					printf "appending new record to db\n\n" 2>&1
					append_records "$zone" "$subdom"
					append_reverse_records "$reverse_zone"
					printf "record installed\n\n" 2>&1
				fi
			else
				printf "appending subdomain to db\n\n" 2>&1
				append_records "$zone" "$subdom"
				append_reverse_records "$reverse_zone"

				printf "subdomain installed\n\n" 2>&1
			fi
			refresh_SOA_serial "$zone" "$reverse_zone"
			restart_remote_named
		else

			if reverse_zone_exists "$reverse_zone"; then
				delete_reverse_subdom "$reverse_zone"
				append_reverse_records "$reverse_zone"
			else
				# make sure that reverse_zone is not repeated in /etc/bind/named.conf.local
				delete_zone "$reverse_zone"
				# recreate zone
				create_new_reverse_zone "$reverse_zone" "$zone"
				create_new_reverse_db "$reverse_zone" "$zone" "$ns1_reverse_zone"
				append_reverse_records "$reverse_zone"
			fi
			# make sure zone is not repeated in named.conf.local
			delete_zone "$zone"
			# recreate zone
			create_new_zone "$zone"
			create_new_records "$zone" "$subdom"
			refresh_SOA_serial "$zone" "$reverse_zone"
			restart_remote_named
			printf "records created\n\n" 2>&1
		fi
	# else if the domain has no subdom and does not exist then use @ instead of subdom
	elif [ "$subdom" = "$dom" ]; then
		echo -e "creating db files and appending zone/reverse zone to $fnamed\n"
	
		if reverse_zone_exists "$reverse_zone"; then
			delete_reverse_subdom "$reverse_zone"
			append_reverse_records "$reverse_zone"
		else
			# make sure that reverse_zone is not repeated in /etc/bind/named.conf.local
			delete_zone "$reverse_zone"
			# recreate zone
			create_new_reverse_zone "$reverse_zone" "$zone"
			create_new_reverse_db "$reverse_zone" "$zone" "$ns1_reverse_zone"
			append_reverse_records "$reverse_zone"
		fi
		# make sure zone is not repeated in named.conf.local
		delete_zone "$zone"
		# recreate zone
		create_new_zone "$zone"
		create_new_records "$zone" "@"

		echo -e "${fnamed} appended with ${zone} && ${reverse_zone} && db.${zone}/d db.${reverse_zone} created\n" 2>&1
		refresh_SOA_serial "$zone" "$reverse_zone"
		restart_remote_named
		printf "zone installed\n\n" 2>&1
	# and finally if the domain does not exist but has a subdom the following
	else
		echo -e "creating db files and appending zone/reverse zone to $fnamed\n" 2>&1

		if reverse_zone_exists "$reverse_zone"; then
			delete_reverse_subdom "$reverse_zone"
			append_reverse_records "$reverse_zone"
		else
			# make sure that reverse_zone is not repeated in /etc/bind/named.conf.local
			delete_zone "$reverse_zone"
			# recreate zone
			create_new_reverse_zone "$reverse_zone" "$zone"
			create_new_reverse_db "$reverse_zone" "$zone" "$ns1_reverse_zone"
			append_reverse_records "$reverse_zone"
		fi
		# make sure zone is not repeated in named.conf.local
		delete_zone "$zone"
		# recreate zone
		create_new_zone "$zone"
		create_new_records "$zone" "$subdom"

		echo -e "${fnamed} appended with ${zone} && ${reverse_zone} && db.${zone}/d db.${reverse_zone} created\n" 2>&1
		refresh_SOA_serial "$zone" "$reverse_zone"
		restart_remote_named
		printf "zone installed\n\n" 2>&1
	fi
	# cleanup hush so other root logins can still see it
	cleanup_hush_remote_motd

	echo "done" 2>&1
	exit 0
}
# -------

# ==================== #
# |code execution|
# ==================== #
# conditions:
if is_not_root; then
	printf 'Please run script as root or via sudo!\n'
	exit 1
	# exit 1 outputs above prinf into stderr
elif [ ! "$dom" ] || [[ $dom == -* ]]; then
	printf 'Please provide a domain\n'
	display_help
	exit 1
elif is_valid; then
	#if [ "$verbose_switch" == "1" ]; then #idk if this will work the way I think it will, update: nope, removed flag
	create_new_log_entry
	main > >(tee -a create_website.log) 2> >(tee -a create_website_error.log >&2) # -> from https://stackoverflow.com/a/692407
	#else
	#	create_new_log_entry
	#	main 2>> create_website.log
	#fi
else
	printf "Domain '%s' is not valid!\n" "$dom"
	exit 1
fi

# -------

# and thus ends the longest bash script that I have ever written... -_-
# --> ok i'll admit its mostly my fault for not using template files and overcomplicating things massively :p
