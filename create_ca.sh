#!/usr/bin/env bash

# [default flag vars]
progname=$(basename $0)
ca_loc=/srv/ca
remote_ca_switch=0
ca_srv=
# -------

# ================ #
# |flags|
# ================ #
# taken and modified from: https://stackoverflow.com/a/39376824

# define help
display_help(){
   cat << END

   Usage: $progname [options]...

   optional arguments:
     -h, --help               show this help message and exit

     --remote-ca IP           enables use of remote ca-server at given IP (note that script expects to be logged in as root)
     --ca-dir DIR             sets the location of the ca (default is: /srv/ca)
END
# -v, --verbose        increase the verbosity of the bash script (shows sterr)
}
# use getopt and store the output into $OPTS
# note the use of -o for the short options, --long for the long name options
# and a : for any option that takes a parameter
OPTS=$(getopt -o "h" --long "help,remote-ca:,ca-dir:" -n "$progname" -- "$@")
if [ $? != 0 ] ; then echo "Error in command line arguments." >&2 ; display_help; exit 1 ; fi
eval set -- "$OPTS"

while true; do
  # uncomment the next line to see how shift is working
  # echo "\$1:\"$1\" \$2:\"$2\""
  case "$1" in
    -h | --help ) display_help; exit; ;;
    --remote-ca ) remote_ca_switch=1 ca_srv="$2"; shift 2 ;;
    --ca-dir ) ca_loc="$2"; shift 2;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done
if [ ! "$ca_loc" ]; then
    ca_loc=/srv/ca
fi
# -------

# ================ #
# |global vars|
# ================ #
ssl_cnf=/usr/lib/ssl/openssl.cnf
r_cert_key=${ca_loc}/private/cakey.pem
r_cert=${ca_loc}/cacert.pem
# -------

# ================ #
# |functions|
# ================ #
is_not_root() {
	! [ "$(id -u)" -eq 0 ]
	# check if command is not run as root (or via sudo), taken from: https://stackoverflow.com/a/52586842
}
hush_remote_motd(){
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1
	fi
}

cleanup_hush_remote_motd(){
	# remove the .hushlogin files so that motd is displayed should root login again
	if [ "$remote_ca_switch" == "1" ]; then
		ssh root@$ca_srv "touch $HOME/.hushlogin" > /dev/null 2>&1
	fi
}

set_ntp(){
    if [ "$remote_ca_switch" == "1" ]; then
        ssh root@$ca_srv "timedatectl set-ntp true"
    else
        timedatectl set-ntp true
    fi
}

create_ca_environment(){
    if [ "$remote_ca_switch" == "1" ]; then
        ssh root@$ca_srv <<- +
            mkdir -p ${ca_loc}/{newcerts,certs,crl,private,requests}
            chown -R root: ${ca_loc}; chmod -R 700 $ca_loc
            touch ${ca_loc}/index.txt
            echo 1000 > ${ca_loc}/serial
            touch /root/.rnd
+
    else
        mkdir -p ${ca_loc}/{newcerts,certs,crl,private,requests}
        chown -R root: ${ca_loc}; chmod -R 700 $ca_loc
        touch ${ca_loc}/index.txt
        echo 1000 > ${ca_loc}/serial
        touch /root/.rnd
    fi
}

edit_ssl_cnf(){
    if [ "$remote_ca_switch" == "1" ]; then
        ssh root@$ca_srv <<- +
            sed -i 's|./demoCA|${ca_loc}|' $ssl_cnf
            sed -i '/\[[[:blank:]]policy_match[[:blank:]]\]/,/\[[[:blank:]]policy_anything[[:blank:]]\]/{s/\bmatch\b/supplied/g}' $ssl_cnf
+
    else
        sed -i "s|./demoCA|${ca_loc}|" $ssl_cnf
        sed -i '/\[[[:blank:]]policy_match[[:blank:]]\]/,/\[[[:blank:]]policy_anything[[:blank:]]\]/{s/\bmatch\b/supplied/g}' $ssl_cnf
    fi
# first sed command will replace all occurances of the str ./demoCa with $ca_loc in ssl_cnf, forward slashes in locations do not need to be escaped if sed is delimited by |, else you get an error when expanding the variable into the sed command
# made with help of: https://linuxize.com/post/how-to-use-sed-to-find-and-replace-string-in-files/
# second sed command will replace the word "match" with supplied only it it occurs after [ policy_match ], (\b means: only if is not part of another string)
# modified from chatgpt's answer (it forgot to \bmatch\b, I also added [[:blank:]] instead of spaces as it allows for tabs, idk if that is needed but whatever)
}
create_ssl_pair(){
     if [ "$remote_ca_switch" == "1" ]; then
        ssh root@$ca_srv <<- +
            openssl genrsa -aes256 -out ${ca_loc}/private/cakey.pem 4096
            openssl req -new -x509 -key ${ca_loc}/private/cakey.pem -out ${ca_loc}/cacert.pem -days 3650 -set_serial 0
+
    else
        openssl genrsa -aes256 -out ${ca_loc}/private/cakey.pem 4096
        openssl req -new -x509 -key ${ca_loc}/private/cakey.pem -out ${ca_loc}/cacert.pem -days 3650 -set_serial 0
    fi
}
# -------

# ================ #
# |code execution|
# ================ #
if is_not_root; then
    echo "run command as root"
    exit 1
elif [ "$remote_ca_switch" == "1" ] && [ ! "$ca_srv" ] ; then
    echo "remote CA ip is missing"
    display_help
    exit 1
else
    echo "enabling ntp"
    set_ntp
    echo "setting up ca environment"
    create_ca_environment
    edit_ssl_cnf
    echo "creating ssl keypair"
    echo "please wait a few seconds while key is generated"
    create_ssl_pair
    echo "done"
fi
