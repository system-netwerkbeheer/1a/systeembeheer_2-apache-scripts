#!/usr/bin/env bash
# [input vars]
dom=$1
# remove www. from domain if given
if [[ $dom == www.* ]]; then
	dom="${dom//www.}"
fi
# [default flag vars]
progname=$(basename $0)
yes_switch=0

# [locations]
html=/var/www/html
enabled=/etc/apache2/sites-enabled
available=/etc/apache2/sites-available
fhtml=${html}/${dom}
fa_enabled=${enabled}/${dom}.conf
fa_available=${available}/${dom}.conf
# [dns]
ns1=192.168.0.20
ns2=192.168.0.21
ns_etc=/etc/bind
fnamed=${ns_etc}/named.conf.local
soa_serial=$(date "+%s") # %s is the UTC timestamp (number of seconds since 1970-01-01 00:00:00 UTC), this should allow easy updating of SOA, so long as you do not spam the script every .5 second...
# [local]
ip=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/') # from https://unix.stackexchange.com/a/119272
lastdigits=$(echo $ip | sed -r 's!/.*!!; s!.*\.!!')
rip="${ip//$lastdigits}" # from https://unix.stackexchange.com/a/311762
# [ssl locations]
ca=/srv/ca
privatedir=/etc/apache2/ssl/private
publicdir=/etc/apache2/ssl/public
# ---

# |flags|
# taken and modified from: https://stackoverflow.com/a/39376824
# define help
display_help(){
   cat << END

   Usage: $progname <domain> [ARGS]...

   optional arguments:
     -h, --help           show this help message and exit
     -y                   answers all yes/no questions with yes
	                      (WILL DELETE ALL APACHE CONFIGS LINKED TO ZONE AND ENTIRE DNS ZONE!)

END
#
}
# use getopt and store the output into $OPTS
# note the use of -o for the short options, --long for the long name options
# and a : for any option that takes a parameter
OPTS=$(getopt -o "hy" --long "help" -n "$progname" -- "$@")
if [ $? != 0 ] ; then echo "Error in command line arguments." >&2 ; display_help; exit 1 ; fi
eval set -- "$OPTS"

while true; do
  # uncomment the next line to see how shift is working
  # echo "\$1:\"$1\" \$2:\"$2\""
  case "$1" in
    -h | --help ) display_help; exit; ;;
    -y ) yes_switch=1; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done
# ---

# [functions]
ask_yes_or_no() {
# a little bit of safety
    read -p "$1 (y/N): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
} # from https://stackoverflow.com/a/16508612

is_not_root() {
	! [ "$(id -u)" -eq 0 ]
	# check if command is not run as root (or via sudo), taken from: https://stackoverflow.com/a/52586842
}

apache_exists(){
	[ -d "/var/www/html/${dom}" ] || [ -f "$fa_available" ]
}
zone_exists() {
	ssh -q root@$ns1 [[ -f ${ns_etc}/db.${1} ]]
}

extract_zone() {
	echo "$1" | grep -Eo '[^.]*.[^.]*$'
} # from https://unix.stackexchange.com/a/711621

extract_reverse_zone() {
	echo "$(printf %s "$rip" | tac -s.)in-addr.arpa" # from https://unix.stackexchange.com/a/132785
}

extract_reverse_ip() {
# same as extract_reverse_zone but without the .in-addr.arp addendum, makes it easier to delete reverse ip db files
	# sed command removes last character in the string (aka the trailing ".")
	echo "$(printf %s "$rip" | tac -s.)" | sed 's/.$//'
} # sed command is from: https://unix.stackexchange.com/a/229994

extract_subdom() {
	echo "$dom" | sed -e "s#.${1}##"
} # sed command modified from https://stackoverflow.com/a/17129081

refresh_SOA_serial(){
	# the SOA record serial number needs to be updated every time the db.zone file is modified so that the slave servers update their cache

	# 	if '' are put arround the delimeter like '+' the variables would not be able to expand -> $1 would litteraly be printed instead of example.net, double quotes or no quotes do allow expansion

	ssh -T root@$ns1 << + 
		awk '{ if ( \$0 ~ /[\t ]SOA[\t ]/ ) \$6=$soa_serial; print}' ${ns_etc}/db.${1} > /tmp/updated_record && mv /tmp/updated_record ${ns_etc}/db.${1}
		awk '{ if ( \$0 ~ /[\t ]SOA[\t ]/ ) \$6=$soa_serial; print}' ${ns_etc}/db.${2} > /tmp/updated_rr_record && mv /tmp/updated_rr_record ${ns_etc}/db.${2}
+
	# awk command will increment the 6th field in the line that contains the word SOA, surrouned by spaces or tabs (so the serial number), by 1
	# it assumes that the SOA entry will be on 1 line, therefore the formatting of db file must be correct
	# --> so soa entry must be: <name> <class> <type> <mname> <rname> <serial> <refresh> <retry> <expire> <minimum>

	# as the awk command cannot edit the file directly? --> didnt find a good answer to this, should read some things about awk but the book I have is a 1000 pages long so fuck that
	# --> it creates a new file in /tmp and then replaces the original
} # awk command from: https://serverfault.com/a/950021


delete_apache(){
	# rm configs
	rm -r "$fhtml"; unlink "$fa_enabled"; rm "$fa_available"
}
delete_apache_zone(){
	# rm configs
	rm -r "${html}/*${1}"; unlink "${enabled}/*${1}"; rm "${available}/*${1}"
}

restart_apache(){
	systemctl restart apache2.service
}
#delete_ssl(){
#	rm ${privatedir}/${dom}.key
#	rm ${privatedir}/${dom}.csr
#	rm ${publicdir}/${dom}.pem
	# rm local ca (comment if remote)
	# rm remote ca (un comment if remote)
	# ssh root@$ca_srv "rm ${ca}/requests/${dom}.csr ${ca}/certs/${dom}.pem"

#} #--> breaks ssl creation (cuz I need to revoke certs?, idk ill just keep all site certs and see if the pem file exists for the site in the script, not really a nice solution but whatever, I dont know much about CA anyway)

hush_remote_motd() {
	# remove the motd on remote ssh, so output is clearer
	ssh root@$ns1 "touch $HOME/.hushlogin" 2>&1 >/dev/null
	ssh root@$ns2 "touch $HOME/.hushlogin" 2>&1 >/dev/null
	#ssh root@$ca_srv "touch $HOME/.hushlogin" 2>&1 >/dev/null
}

cleanup_hush_remote_motd(){
	# remove the .hushlogin files so that motd is displayed should root login again
	ssh root@$ns1 "rm $HOME/.hushlogin" 2>&1 >/dev/null
	ssh root@$ns2 "rm $HOME/.hushlogin" 2>&1 >/dev/null
	# ssh root@$ca_srv "rm $HOME/.hushlogin" 2>&1 >/dev/null
}

delete_zone() {
	# <<+ is a delimiter that will push all text until + into ssh as commands line by line, reason i needed this is cuz there is a lot of ' and " which made the normal method not work
	# the sed command removes the domain-zone from /etc/bind/named.conf.local 
	ssh -T root@$ns1 <<+
		sed -nie "/\"$1\"/,/^\};"'$/d;p;' $fnamed
+
	ssh -T root@$ns2 <<+
		sed -nie "/\"$1\"/,/^\};"'$/d;p;' $fnamed
+
# sed -nie "/\"${2}\"/,/^\};"'$/d;p;' $fnamed
} # method for delimiting from: https://stackoverflow.com/a/27413675; first sed command from: did not save source :(, second was a version of first modified by chatgpt to only delete the lines if they contain db.{2}.rr (sed -nie '/"'${1}.${2}'"/,/^};$/ { /db\.'"$1"'\.rr/!b; d }; p' $fnamed)
# --> update: simplified it by just using $reverse_zone once for each subnet

delete_subdom() {
	ssh -T root@$ns1 << +
		grep -v "${2} IN A\|www.${dom}. IN CNAME ${dom}." ${ns_etc}/db.${1} > /tmp/db.${1}.tmp; mv /tmp/db.${1}.tmp ${ns_etc}/db.${1}
+
} # grep modified from: https://stackoverflow.com/a/13188531

delete_reverse_subdom(){
	ssh -T root@$ns1 << +
		grep -v "IN PTR ${dom}." ${ns_etc}/db.${1} > /tmp/db.${1}.tmp; mv /tmp/db.${1}.tmp ${ns_etc}/db.${1}
+
} # grep modified from: https://stackoverflow.com/a/13188531

delete_db() {
	ssh root@$ns1 "rm ${ns_etc}/db.${1}"
}

restart_remote_named(){
	ssh root@$ns1 'systemctl restart named.service'
	ssh root@$ns2 'systemctl restart named.service'
}
# ---
if is_not_root; then
	printf 'Please run script as root or via sudo!\n'
	exit 1
	# exit 1 outputs above prinf into stderr
elif [ ! "$dom" ] || [[ $dom == -* ]]; then
	printf 'Please provide a domain\n'
	display_help
	exit 1
else

	# [vars]
	zone=$(extract_zone "$dom")
	reverse_zone=$(extract_reverse_zone)
	subdom=$(extract_subdom "$zone")
	# ---

	# ask for confirmation
	if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Are you sure you wish to delete the given domain?") ]]; then
	# if no then exit program
		echo "OK, exiting."
		exit 0
	else
		hush_remote_motd
		if apache_exists; then
			# if yes then do the following:
			echo "deleting apache configs"
			#delete_ssl
			delete_apache
			echo -e "apache configs deleted, note that other configs for the zone might still exist.\nlisting currently enabled:\n"
			ls /etc/apache2/sites-enabled | grep -i "$zone"
			echo ""
			if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to delete the all other subomains in ending in ${zone}?") ]]; then
				echo "ok, existing configs linked to $zone preserved"
			else
				delete_apache_zone "$zone"
				echo "all apache configs for $zone deleted"
			fi
		else
			echo "apache config does not exist, skipping to DNS zone deletion"
		fi
		# clean dns configuration
		# -> zones
		if zone_exists "$zone"; then # check if the db.zone file exists, then ask if you wish to delete that or just the given domain
			echo "zone $zone exists in remote dns"
			# check if domain has a subdom or not, if it has not delete zone, else ask to delete zone or only subdomain
			# --> have not tested this with domains with 2nd subdomain like www.web.pengolodh.be, idk if that is needed for a one of
			if [ "$subdom" = "$dom" ]; then
				if [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to delete the entire DNS zone (${zone})?") ]]; then
					echo "removing A, CNAME and PTR records for $dom from remote dns config"
					delete_subdom  "$zone" "@"
					delete_reverse_subdom "$reverse_zone"
					refresh_SOA_serial "$zone"  "$reverse_zone"
					restart_remote_named
					echo "done"
					exit 0
				else
					echo "deleting DNS zone $zone"
					# delete zones from named.conf.local
					delete_zone "$zone"
					delete_reverse_subdom "$reverse_zone"
					# delete db files and restart named
					delete_db "$zone"
					restart_remote_named
					echo "done"
					exit 0
				fi
			elif [ "$yes_switch" == "0" ] && [[ "no" == $(ask_yes_or_no "Do you wish to delete the entire DNS zone (db.${zone}) [y]/ or only the subdomain (${subdom}) [N]?") ]]; then
				echo "removing A, CNAME and PTR records for $dom from remote dns config"
				delete_subdom  "$zone" "$subdom"
				delete_reverse_subdom "$reverse_zone"
				refresh_SOA_serial "$zone"  "$reverse_zone"
				restart_remote_named
				echo "done"
				exit 0
			else

				echo "deleting DNS zone $zone"
				# delete zones from named.conf.local
				delete_zone "$zone"
				delete_reverse_subdom "$reverse_zone"
				# delete db files and restart named
				delete_db "$zone"
				restart_remote_named
				echo "done"
				exit 0
			fi
		else
			echo "remote DNS db file does not exist, cleaning finished"
			exit 0
		fi
	fi
fi
