#!/usr/bin/env bash
ask_yes_or_no() {
# a little bit of safety
    read -p "$1 (y/N): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
} # from https://stackoverflow.com/a/16508612
is_not_root(){
	! [ "$(id -u)" -eq 0 ]
	# check if command is not run as root (or via sudo), taken from: https://stackoverflow.com/a/52586842
}
if is_not_root; then
    echo "run script as root"
    exit 0
fi
echo "this script deletes ALL installed certificates exept the root cert on the local server"
if [[ "no" == $(ask_yes_or_no "Do you wish to continue?") ]]; then
    echo "ok, exiting..."
    exit 0
else
    rm -r /etc/apache2/ssl/* /srv/ca/newcerts/*
    rm /srv/ca/index.* /srv/ca/serial
    touch /srv/ca/index.txt
    echo "1000" > /srv/ca/serial
    echo "certs deleted..., run create website script again if you wish to regenerate them for a particular domain"
    exit 0
fi